<?php
declare(strict_types = 1);
class Book extends Author
{
    private string $name_book;
    private int $year_edition;

    public function getAge()
    {
        return $this->age;
    }

    public function setAge($age)
    {
        $this->age = $age;
    }

    public function name_book()
    {
        return $this->name_book;
    }

    public function  getName_book()
    {
        return $this->name_book;
    }
    public function setName_book($name_book)
    {
        $this->name_book = $name_book;
    }

    public function getYear_edition()
    {
        return $this->year_edition;
    }

    public function setYear_edition($year_edition)
    {
        $this-> year_edition= $year_edition;
    }


}
?>