<?php
declare(strict_types = 1);

class Author
{
    protected string $name;
    protected int $age;
    public function __construct(string $name,int $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {

        $this->name = $name;
    }



}
?>