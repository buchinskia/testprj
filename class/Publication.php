<?php
declare(strict_types = 1);
class Publication extends Book
{
   private $name_pbl;
   private $year_pbl;

   public function __construct(string $name_pbl, int $year_pbl)
   {

       $this->name_pbl=$name_pbl;
       $this->year_pbl = $year_pbl;
   }

    /**
     * @return mixed
     */
    public function getNamePbl()
    {
        return $this->name_pbl;
    }
    public function setName_pbl($name_pbl)
    {
        $this->name_pbl = $name_pbl;
    }
    public function getYear_pbl()
    {
      return $this->year_pbl;
    }
    public function setYear_pbl($year_pbl)
    {
     $this->year_pbl=$year_pbl;
    }

}
?>