<?php
class Author
{
    protected $name;
    protected $age;
    public function __construct($name,  $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {

        $this->name = $name;
    }
    private function callToPrivateName()
    {
        return "{$this->name}";
    }

}
class Book extends Author
{
    public $name_book;
    public $year_edition;

    public function getAge()
    {
        return $this->age;
    }

    public function setAge($age)
    {
        $this->age = $age;
    }

    public function name_book()
    {
        return $this->name_book;
    }

    public function  getName_book()
    {
        return $this->name_book;
    }
    public function setName_book($name_book)
    {
        $this->name_book = $name_book;
    }

    public function getYear_edition()
    {
        return $this->year_edition;
    }

    public function setYear_edition($year_edition)
    {
        $this-> year_edition= $year_edition;
    }

    public function getNameAndAge()
    {
        return $this->callToProtectedName();
    }
}

$employee = new Book();

$employee->setName('Bob Smith');
$employee->setAge(30);
$employee->setName_book('Software coding');
$employee->setYear_edition('30');

echo $employee->getName(); // prints 'Bob Smith'
echo $employee->getAge(); // prints '30'
echo $employee->getName_book(); // prints 'Software coding'
echo $employee->getYear_edition(); // prints '30

?>